# Serializers define the API representation.
from rest_framework import serializers

from members.models import Member


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'telegram_id', 'first_name', 'last_name', 'username')
