from rest_framework import mixins, viewsets

from members.models import Member
from members.serializers import MemberSerializer


class MemberViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
