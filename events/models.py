from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from groups.models import Membership


class Event(models.Model):
    SUCCESS = 'SUCCESS'
    ABSENT = 'ABSENT'
    SKIP = 'SKIP'
    name_choices = (
        (SUCCESS, 'Sucesso'),
        (ABSENT, 'Ausente'),
        (SKIP, 'Passar'),
    )

    membership = models.ForeignKey(Membership, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, choices=name_choices)
    created_at = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=Event)
def after_event_created(sender, instance, **kwargs):
    membership = instance.membership
    membership.skips += 1 if instance.name == "SKIP" else 0
    membership.absentions += 1 if instance.name == "ABSENT" else 0
    membership.success += 1 if instance.name == "SUCCESS" else 0
    membership.success -= 1 if instance.name == "DECREASE" else 0
    membership.save()
