# Serializers define the API representation.
from rest_framework import serializers

from events.models import Event
from groups.serializers import MembershipSerializer


class EventSerializer(serializers.ModelSerializer):
    membership = MembershipSerializer()

    class Meta:
        model = Event
        fields = ('membership', 'name', 'created_at')
