from pprint import pprint

from django.forms import model_to_dict
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.response import Response

from events.models import Event
from events.serializers import EventSerializer
from groups.models import Membership


class EventViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    @action(detail=True, url_name='event',
            url_path='event(?P<group_telegram_id>[0-9]+)&(?P<member_telegram_id>[0-9]+)&(?P<member_username>[a-z0-9]+)')
    def post(self, request, pk=None):
        group = request.query_params['group_telegram_id']
        member = None
        membership_params = {'group__telegram_id': group}

        if 'member_telegram_id' in request.query_params:
            member = request.query_params['member_telegram_id']
            membership_params['member__telegram_id'] = member
        elif 'member_username' in request.query_params:
            member = request.query_params['member_username']
            membership_params['member__username'] = member

        name = request.data['name']

        if not (group and member):
            raise NotFound()
        if not name:
            raise ValidationError()

        membership = Membership.objects.filter(**membership_params).first()
        if not membership:
            raise NotFound()

        event = membership.event_set.create(name=name)
        serializer = EventSerializer(event)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
