# Serializers define the API representation.
from rest_framework import serializers
from groups.models import Group, Membership
from members.models import Member
from members.serializers import MemberSerializer


class GroupSerializer(serializers.ModelSerializer):
    members = MemberSerializer(many=True)

    class Meta:
        model = Group
        fields = ('id', 'telegram_id', 'title', 'members')


class SimpleGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id', 'telegram_id', 'title')


class MembershipSerializer(serializers.ModelSerializer):
    member = MemberSerializer()
    group = SimpleGroupSerializer()
    score = serializers.SerializerMethodField('_get_score')

    def _get_score(self, obj):
        return obj.success

    def create(self, validated_data):
        member = Member.objects.filter(telegram_id=validated_data['member']['telegram_id']).first()
        group = Group.objects.filter(telegram_id=validated_data['group']['telegram_id']).first()

        if not member:
            member = Member.objects.create(**validated_data['member'])
        if not group:
            group = Group.objects.create(**validated_data['group'])

        membership = Membership.objects.filter(member_id=member.id, group_id=group.id).first()

        if membership:
            membership.disabled = False
            membership.save()
            return membership

        instance = Membership.objects.filter(member_id=member.id, group_id=group.id).first()
        if not instance:
            instance = Membership.objects.create(member=member, group=group, success=0, skips=0, absentions=0)
        return instance

    class Meta:
        model = Membership
        fields = ('group', 'member', 'skips', 'absentions', 'success', 'disabled', 'score')
