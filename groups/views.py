import json

from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response

from groups.models import Group, Membership
from groups.serializers import GroupSerializer, MembershipSerializer


class GroupViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    @action(methods=['GET'], detail=False, url_name='get-next',
            url_path='(?P<group_telegram_id>[-0-9]+)/get_next')
    def get_next(self, request, group_telegram_id, pk=None):
        group = Group.objects.filter(telegram_id=group_telegram_id).first()
        if group:
            data = group.get_next()
            serializer = MembershipSerializer(data)
            return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=['GET'], detail=False, url_name='ranking',
            url_path='(?P<group_telegram_id>[-0-9]+)/ranking')
    def ranking(self, request, group_telegram_id, pk=None):
        group = Group.objects.filter(telegram_id=group_telegram_id).first()
        data = group.get_ranking()
        serializer = MembershipSerializer(data, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class MembershipViewSet(mixins.RetrieveModelMixin,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer

    @action(detail=True, url_name='membership',
            url_path='membership(?P<group_telegram_id>[0-9]+)&P<member_telegram_id>[0-9]+')
    def delete(self, request: Request, pk=None):
        group = request.query_params['group_telegram_id']
        member = request.query_params['member_telegram_id']
        membership = Membership.objects.filter(group__telegram_id=group, member__telegram_id=member).first()

        if membership:
            membership.disabled = True
            membership.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
