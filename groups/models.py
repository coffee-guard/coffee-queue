from django.db import models

from members.models import Member

from django.db.models import Q

from datetime import datetime, timedelta


class Group(models.Model):
    telegram_id = models.CharField(max_length=128)
    title = models.CharField(max_length=128)
    members = models.ManyToManyField(Member, through='Membership')

    def get_next(self):
        from events.models import Event

        today = datetime.now().date()
        exclude_filter = {
            "event__name": 'ABSENT',
            "event__created_at__gt": today,
            "disabled": True
        }

        available = self.membership_set.filter(
            ~(Q(event__name='ABSENT') & Q(event__created_at__gt=today)) & Q(disabled=False)
        )

        last_event = Event.objects.filter(membership__group=self.id).last()
        if last_event:
            available = available.exclude(member_id=last_event.membership.member_id)

        return available.extra(order_by=['success']).first()
        # return available.extra(select={'score': 'success - skips'}).extra(order_by=['score']).first()

    def get_ranking(self):
        return self.membership_set.order_by('-success', 'skips', 'absentions')


class Membership(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    skips = models.PositiveIntegerField(default=0)
    absentions = models.PositiveIntegerField(default=0)
    success = models.IntegerField(default=0)
    disabled = models.BooleanField(default=False)

    @property
    def score(self):
        return self.success
        # return self.success - self.skips
