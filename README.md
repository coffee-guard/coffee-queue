# Coffe queue
Back-end do guardião da cafeteira

## Documentação do framework

* Django - https://docs.djangoproject.com/en/2.2/
* Django  Rest framework - https://www.django-rest-framework.org/

## Autenticação <span style="color:red;"> - Ainda não Implementado - </span>
### Criando usuario e token
```
python manage.py createsuperuser --username renatolb --email renatolb123@gmail.com
python manage.py drf_create_token renatolb
```
### Header de autenticação
```Authorization: Token 9054f7aa9305e012b3c2300408c3dfdf390fcddf```
